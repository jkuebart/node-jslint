Douglas Crockford's JSLint
==========================

This package provides Douglas Crockford's [JSLint][JL] library as a Node.js
package. The package version indicates the upstream »edition«.

Both a universal CommonJS/AMD module and an ECMAScript 6 module are
provided.

Notable features of this package are:
  * its major version number is equal to the JSLint »edition«
  * it is automatically updated when a new edition of JSLint is released
  * it contains unaltered JSLint source code
  * the code is itself JSLint-clean


Versioning
----------

The major version number of this package indicates the JSLint »edition« it
contains. Multiple updates to JSLint on a single day are represented by
increasing minor version numbers. The patch version indicates updates to
this package itself.


Usage from Node.js
------------------

This package can be installed using

    $ npm install @jkuebart/jslint

The `jslint` function expects up to three arguments `source`,
`option_object` and `global_array` as explained in the [JSLint
documentation][JLF].

```javascript
const jslint = require("@jkuebart/jslint");

const report = jslint(source, option_object, global_array);
```

The `report` module which aids in formatting JSLint reports as HTML is also
included and can be loaded like this:

```javascript
const jslintReport = require("@jkuebart/jslint/dist/report.min.js");
```

For more detailed examples, see the [documentation][JLD].


Usage from HTML
---------------

This package makes it [quite simple][D] to create a basic website similar
to [JSLint][JL]'s.

```html
<!DOCTYPE html>
<meta charset="utf-8">

<title>JSLint Node package demo page</title>

<script type="module">
    import jslint from "//unpkg.com/@jkuebart/jslint/dist/jslint.min.js";
    import report from "//unpkg.com/@jkuebart/jslint/dist/report.min.js";

    const $ = document.querySelectorAll.bind(document);

    document.addEventListener("DOMContentLoaded", function () {
        const error = $("#error")[0];
        const source = $("#source")[0];
        const update = $("#update")[0];

        update.onclick = function update() {
            error.innerHTML = report.error(jslint(source.value));
        }
    });
</script>

<div><textarea id="source"></textarea></div>
<button id="update">JSLint</button>
<pre id="error"></pre>
```

If you're having to support browsers which can't handle ES6 modules, you
can fall back to the universal modules:

```html
<!DOCTYPE html>
<meta charset="utf-8">

<title>JSLint Node package demo page</title>

<script src="//unpkg.com/@jkuebart/jslint/dist/jslint.umd.min.js"></script>
<script src="//unpkg.com/@jkuebart/jslint/dist/report.umd.min.js"></script>
<script>
    var $ = document.querySelectorAll.bind(document);

    document.addEventListener("DOMContentLoaded", function () {
        var error = $("#error")[0];
        var source = $("#source")[0];
        var update = $("#update")[0];

        update.onclick = function update() {
            error.innerHTML = report.error(jslint(source.value));
        }
    });
</script>

<div><textarea id="source"></textarea></div>
<button id="update">JSLint</button>
<pre id="error"></pre>
```

Alternatively, CommonJS and AMD compatible loaders can also load the
universal module.



Using JSLint as a JavaScript parser
-----------------------------------

Apart from a list of warnings, JSLint also returns a full
[abstract syntax tree][AST] of the given code. This can be useful for
automatic refactorings, static analysis, and much more…

```javascript
const jslint = require("@jkuebart/jslint");

function dumpTree(tree) {
    // ...
}

const report = jslint(source, option_object, global_array);
dumpTree(report.tree);
```


Updating
--------

A shell script is provided for building new packages when this project or
the upstream project is updated. It can be run using

    npm run editions

This creates branches and tags based on the »edition« of the upstream
project. Packages still need to be generated and published manually.

The local branches and tags can be viewed using

    npm run show-branches
    npm run show-tags

This can be used to automate some tasks, for example:

    npm run show-branches --silent |
    while read b
    do
        git push --set-upstream origin "${b#refs/heads/}:${b#refs/heads/}"
    done

or

    npm run show-tags --silent |
    while read t
    do
        git checkout "${t#refs/tags/}"
        npm publish --access public
    done

To easily remove automatically created local branches and tags, use

    npm run reset

There is also a shell script that determines whether the upstream project
has new commits.

    npm run show-branches --silent |
    npm run uptodate --silent


[AST]: http://en.wikipedia.org/wiki/Abstract_syntax_tree
[D]: html/jslint.html
[JLD]: http://jslint.com/help.html
[JLF]: http://jslint.com/function.html
[JL]: http://jslint.com/
