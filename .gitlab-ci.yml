image: 'node'

variables:

# Setup committer identity.

    GIT_AUTHOR_NAME: 'Automatic update job'
    GIT_AUTHOR_EMAIL: 'incoming+jkuebart/node-jslint@incoming.gitlab.com'
    GIT_COMMITTER_NAME: 'Automatic update job'
    GIT_COMMITTER_EMAIL: 'incoming+jkuebart/node-jslint@incoming.gitlab.com'

# In order to be able to push, we clone using ssh rather than the default
# https.

    GIT_STRATEGY: 'none'

update:

# Triggered by a webhook from the upstream repository.

    only:
        refs:
          - 'triggers'

    script: |
        # Provide ssh credentials.
        mkdir -p ~/.ssh
        printf '%s\n' "$SSH_PRIVATE_KEY" > ~/.ssh/id_rsa
        printf '%s\n' "$SSH_KNOWN_HOSTS" >> ~/.ssh/known_hosts
        chmod -R go-rwx ~/.ssh

        # Allow running build scripts as root and provide npm credentials.
        echo "unsafe-perm = true" >> ~/.npmrc
        echo "//registry.npmjs.org/:_authToken=$NPM_TOKEN" >> ~/.npmrc

        # Clone via ssh.
        git clone --branch "$CI_COMMIT_REF_NAME" "ssh://git@${CI_REPOSITORY_URL#*@}" .
        git submodule update --init -- JSLint

        # Stop if at least one branch contains the latest changes to
        # relevant files.
        if
            git for-each-ref --format='%(refname)' 'refs/remotes/origin/v????????' |
            npm run uptodate --silent
        then
            exit 0
        fi

        # Commit any updates to dependencies.
        rm -fr node_modules package-lock.json
        npm install
        if ! git update-index --refresh
        then
            git commit --all --message='Update dependencies.'
        fi

        # o Merge changes to this project's main into all existing versions.
        # o Create new and update existing versions for new upstream
        #   editions.
        npm run reset
        npm run editions

        # Push changes.
        npm run show-branches --silent |
        while read b
        do
            git push --set-upstream origin "${b#refs/heads/}:${b#refs/heads/}"
        done
        git push --tags origin "$CI_COMMIT_REF_NAME"

        # Build and publish new packages.
        #
        # Due to npm's automatic 'latest' tag handling, it's important that
        # the newest version is published last. The tags are sorted
        # alphabetically, so this is only the case as long as the minor
        # version is just a single digit, i.e. at most 10 editions on a
        # single day.
        npm run show-tags --silent |
        while read t
        do
            git checkout "$t"
            npm publish --access public
        done

        # Kick off next stage.
        curl \
            -X POST \
            -F token="$CI_JOB_TOKEN" \
            -F ref=main \
            'https://gitlab.com/api/v4/projects/4609459/trigger/pipeline'

    stage: 'build'
